package com.wby.pocket.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.wby.pocket.pojo.Default;
import com.wby.pocket.service.DefaultService;

@Controller
@RequestMapping("/user")
public class DefaultController {
 
	@Resource
	private  DefaultService service;
	
	@RequestMapping("showUser")
	public String toIndex(HttpServletRequest request,Model model){
       int id = Integer.parseInt(request.getParameter("id"));
       Default user =   service.getDefaultById(id);
	   model.addAttribute("user", user);
	   
       return "showUser";
	} 
	
		
}
