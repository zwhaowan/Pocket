package com.wby.pocket.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.wby.pocket.dao.DefaultDao;
import com.wby.pocket.pojo.Default;
import com.wby.pocket.service.DefaultService;

@Service("userService")
public class DefaultServiceImpl  implements DefaultService{

	@Resource
	private DefaultDao defaultDao;
		
	@Override
	public Default getDefaultById(int id) {
		
		return this.defaultDao.selectByPrimaryKey(id);
	}
  
	
}
