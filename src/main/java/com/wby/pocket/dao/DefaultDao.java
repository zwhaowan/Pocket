package com.wby.pocket.dao;

import com.wby.pocket.pojo.Default;

public interface DefaultDao {

	int deleteByPrimaryKey(Integer id);

	int insert(Default record);

	int insertSelective(Default record);

	Default selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Default record);

	int updateByPrimaryKey(Default record);
}
