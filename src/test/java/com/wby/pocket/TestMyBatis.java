package com.wby.pocket;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.alibaba.fastjson.JSONObject;
import com.wby.pocket.pojo.Default;
import com.wby.pocket.service.DefaultService;

//表示继承了SpringJUnit4ClassRunner类
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-mybatis.xml")

public class TestMyBatis {

	private static  Logger logger = Logger.getLogger(TestMyBatis.class);
    
	@Resource
	private DefaultService defaultService;
    
	@Test
	public void test1(){
	    Default  user = defaultService.getDefaultById(1);
	    
	    logger.info(JSONObject.toJSONString(user));
		
	}
	
}